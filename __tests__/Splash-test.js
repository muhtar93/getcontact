import React from 'react'
import { render } from '@testing-library/react-native'
import renderer from 'react-test-renderer'
import Splash from '../src/screens/splash/Splash'

jest.mock('react-native-modal', () => {
  const { View } = require('react-native');
  return {
    ReactNativeModal: jest.fn().mockImplementation((props) => {
      return <View>{props.children}</View>;
    }),
  }
})

jest.mock('../src/hooks', () => ({
  useSplash: () => [jest.fn()],
}))

describe('Splash Screen', () => {
  test('renders correctly', () => {
    const tree = renderer.create(<Splash />).toJSON();
    expect(tree).toMatchSnapshot();
  })

  test('renders text and image correctly', () => {
    const { getByText, getByTestId } = render(<Splash />)

    const headlineText = getByText('Get Contact')
    expect(headlineText).toBeTruthy()

    const splashImage = getByTestId('splash-image')
    expect(splashImage).toBeTruthy()
  })

  test('navigate after splash screen', async () => {
    const mockNavigation = { reset: jest.fn() }

    expect(mockNavigation.reset).toHaveBeenCalledTimes(0)
  })
})
