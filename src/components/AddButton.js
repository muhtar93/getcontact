import React from 'react'
import { TouchableOpacity, Text, Image, View } from 'react-native'
import { Colors, Images } from '../res'
import { ContainerStyle, FontStyle, IconStyle } from '../styles'
import Gap from './Gap'

const AddButton = ({onPress}) => {
  return (
    <TouchableOpacity 
      onPress={onPress}
      style={ContainerStyle.containerFlexRowCenter}>
      <View style={[ContainerStyle.containerAddButton, ContainerStyle.containerFlexRowCenter]}>
        <Text style={FontStyle.titleBrown}>Add Contact</Text>
        <Gap width={8} />
        <Image
          source={Images.icAddContact}
          style={IconStyle.smallIcon}
          tintColor={Colors.brown}
        />
      </View>
    </TouchableOpacity>
  )
}

export default AddButton
