import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import { ContainerStyle, FontStyle } from '../styles'

const Button = ({ onPress, title, bgColor, textColor, disabled }) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      disabled={disabled}
      style={[ContainerStyle.containerButton, { backgroundColor: bgColor }]}>
      <Text style={[FontStyle.titleDefault, { color: textColor }]}>{title}</Text>
    </TouchableOpacity>
  )
}

export default Button