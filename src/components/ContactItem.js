import React from 'react'
import { TouchableOpacity, Text, Image, View } from 'react-native'
import { Colors, Images } from '../res'
import { ContainerStyle, FontStyle, IconStyle } from '../styles'
import Gap from './Gap'
import Divider from './Divider'

const ContactItem = ({ onPress, name, onPressDelete, onPressEdit }) => {
  return (
    <View>
      <Gap height={4} />
      <TouchableOpacity
        onPress={onPress}
        style={[ContainerStyle.containerFlexRow, ContainerStyle.containerSpaceBetween]}>
        <Text style={FontStyle.title}>{name}</Text>
        <View style={ContainerStyle.containerFlexRow}>
          <TouchableOpacity onPress={onPressEdit}>
            <Image
              source={Images.icEditContact}
              style={IconStyle.smallIcon}
              tintColor={Colors.blue}
            />
          </TouchableOpacity>
          <Gap width={8} />
          <TouchableOpacity onPress={onPressDelete}>
            <Image
              source={Images.icDeleteContact}
              style={IconStyle.smallIcon}
              tintColor={Colors.red}
            />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
      <Divider />
    </View>
  )
}

export default ContactItem
