import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Colors } from '../res'

const Divider = () => {
  return (
    <View style={styles.divider} />
  )
}

const styles = StyleSheet.create({
  divider: {
    backgroundColor: Colors.brown, 
    height: 0.5, 
  },
})

export default Divider