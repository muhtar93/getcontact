import React from 'react'
import { StyleSheet, TextInput } from 'react-native'
import {Colors, Fonts} from '../res'
import { ContainerStyle } from '../styles'

const EditText = ({ placeholder, value, onChangeText, keyboardType }) => {
  return (
    <TextInput
      autoCapitalize='none'
      style={ContainerStyle.containerEditText}
      placeholder={placeholder}
      value={value}
      placeholderTextColor={Colors.gray}
      onChangeText={onChangeText}
      keyboardType={keyboardType}
    />
  )
}

export default EditText
