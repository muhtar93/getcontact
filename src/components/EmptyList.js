import React from 'react'
import { Text, View, Image } from 'react-native'
import { ContainerStyle, FontStyle, IconStyle } from '../styles'
import { Colors } from '../res'
import { Gap } from '.'

const EmptyList = ({source, message}) => {
  return (
    <View style={ContainerStyle.containerEmptyData}>
      <Image
        source={source}
        style={IconStyle.largeIcon} 
        tintColor={Colors.secondary}
      />
      <Gap height={16} />    
      <Text style={FontStyle.titleSecondary}>{message}</Text>
    </View>
  )
}

export default EmptyList