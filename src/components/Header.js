import React from 'react'
import { Text, Image, View, TouchableOpacity } from 'react-native'
import { Colors, Images } from '../res'
import { ContainerStyle, FontStyle, IconStyle } from '../styles'
import Gap from './Gap'

const Header = ({onPress, title}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Gap height={16} />
      <View
        style={ContainerStyle.containerFlexRow}>
        <Image
          source={Images.icBack}
          style={IconStyle.smallIcon}
          tintColor={Colors.secondary}
        />
        <Gap width={8} />
        <Text style={FontStyle.titleSecondary}>{title}</Text>
      </View>
    </TouchableOpacity>
  )
}

export default Header
