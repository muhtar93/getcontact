import React from 'react'
import { Text } from 'react-native'
import { FontStyle } from '../styles'

const Label = ({ title }) => {
	return (
		<Text style={FontStyle.label}>{title}</Text>
	)
}

export default Label