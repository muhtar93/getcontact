import React from 'react'
import { Image, TouchableOpacity, View } from 'react-native'
import { Colors, Images } from '../res'
import Gap from './Gap'
import { ContainerStyle, IconStyle } from '../styles'

const LoadingList = () => (
  <TouchableOpacity
    style={ContainerStyle.containerLoadingShimmer}>
    <View
      style={ContainerStyle.containerTitleShimmer}
    />
    <View style={ContainerStyle.containerFlexRow}>
      <Image
        source={Images.icEditContact}
        style={IconStyle.smallIcon}
        tintColor={Colors.blue}
      />
      <Gap width={8} />
      <Image
        source={Images.icDeleteContact}
        style={IconStyle.smallIcon}
        tintColor={Colors.red}
      />
    </View>
  </TouchableOpacity>
)

export default LoadingList 