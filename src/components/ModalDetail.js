import React from 'react'
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native'
import Modal from 'react-native-modal'
import Gap from './Gap'
import { Colors, Images } from '../res'
import { ContainerStyle, FontStyle, IconStyle } from '../styles'

const ModalDetail = ({ isVisible, source, onBackdropPress, name, age, onPressClose }) => {
  return (
    <Modal
      useNativeDriver
      animationInTiming={100}
      animationOutTiming={300}
      isVisible={isVisible}
      style={styles.container}
      onBackdropPress={onBackdropPress}>
      <View style={ContainerStyle.containerModal}>
        <TouchableOpacity
          onPress={onPressClose}
          style={[ContainerStyle.containerFlexRow, { alignItems: 'center' }]}>
          <Image
            source={Images.icClose}
            style={IconStyle.verySmallIcon}
            tintColor={Colors.secondary}
          />
          <Gap width={16} />
          <Text style={[FontStyle.titleSecondary]}>Contact Detail</Text>
        </TouchableOpacity>
        <Gap height={8} />
        <Image
          source={source}
          style={ContainerStyle.containerRadius}
        />
        <Gap height={8} />
        <View style={{ alignItems: 'center' }}>
          <Text style={FontStyle.titleSecondary}>{name}</Text>
          <Text style={FontStyle.title}>{age}</Text>
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    margin: 0
  },
  containerModal: {
    backgroundColor: 'white',
    width: null,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingHorizontal: 16,
    paddingVertical: 24
  },
  containerOption: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  line: {
    height: 1,
    // backgroundColor: Colors.outerSpace,
    marginTop: 16
  },
  iconModal: {
    width: 24,
    height: 24,
    // tintColor: Colors.danger
  },
  imgModal: {
    height: 200,
    marginTop: 24,
    width: '100%'
  },
  textModal: {
    fontSize: 16,
    // color: Colors.step10,
    // fontFamily: Fonts.INTER_REGULAR,
    textAlign: 'center',
    marginLeft: 24
  }
})

export default ModalDetail