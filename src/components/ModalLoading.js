import React from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native'
import Modal from 'react-native-modal'
import { Colors } from '../res'

const ModalLoading = ({ visible }) => {
  return (
    <Modal
      useNativeDriver={true}
      isVisible={visible}>
      <View style={styles.containerModalHapus}>
        <ActivityIndicator size='large' color={Colors.primary} />
      </View>
    </Modal>
  )
}

export default ModalLoading

const styles = StyleSheet.create({
  containerModalHapus: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
})