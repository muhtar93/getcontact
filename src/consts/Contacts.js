const Contacts = [
  'https://img.freepik.com/free-photo/real-people-natural-portrait-happy-guy-smiling-laughing-looking-upbeat-camera-standing-glasses-white-background_1258-65662.jpg?t=st=1713526640~exp=1713530240~hmac=a31b2a1a5cf6b94768c04b5a6e33be6394c9a4ffee840b48aa6b66e6a22fa5cb&w=1480', 
  'https://img.freepik.com/free-photo/waist-up-portrait-handsome-serious-unshaven-male-keeps-hands-together-dressed-dark-blue-shirt-has-talk-with-interlocutor-stands-against-white-wall-self-confident-man-freelancer_273609-16320.jpg?t=st=1713526584~exp=1713530184~hmac=0289f394f0fe8239a4ba4c478319cc278c5df88dc07508509db852420710b049&w=1480', 
  'https://img.freepik.com/free-photo/brunette-man-t-shirt-posing-while-looking-camera-looking-pleased-front-view_176474-64660.jpg?t=st=1713526648~exp=1713530248~hmac=1913b94faaf48b1e1981efff4ec8decd9fb01c446b11d479766344c01a91e15c&w=1480', 
  'https://img.freepik.com/free-photo/happy-laughing-young-handsome-southeast-asian-man-isolated-blue-studio-background_74952-3619.jpg?t=st=1713526747~exp=1713530347~hmac=fd26aab974995c6b6713a5c172d049e8880e06958c289f936f7704fef5f8e6dd&w=1480', 
  'https://img.freepik.com/free-photo/business-finance-people-concept-enthusiastic-handsome-asian-male-office-worker-employee-with-teeth-braces-showing-thumbs-up-approval-recommend-company-guarantee-quality_1258-57160.jpg?t=st=1713526764~exp=1713530364~hmac=779912ce7c468d34d64a96144fdea226285852eff91a9bcd084949427d3c03db&w=1480',
  'https://img.freepik.com/free-photo/image-young-asian-woman-company-worker-glasses-smiling-holding-digital-tablet-standing-white-background_1258-89376.jpg?t=st=1713526853~exp=1713530453~hmac=fa22077f73b4acee86c70dd76aea005b6dd8432637c6364b9d4a6f9448d911eb&w=1480',
  'https://img.freepik.com/free-photo/young-woman-white-shirt-pointing-up_1150-27592.jpg?t=st=1713525952~exp=1713529552~hmac=eefbeb36b99f62c74b01e81e2396f98bff0908a9f370fab247babe87a4ce6728&w=740',
  'https://img.freepik.com/free-photo/shopping-concept-portrait-attractive-korean-girl-yellow-sweater-showing-promotion-offer-copy-space-pointing-looking-left-with-pleased-smile-blue-background_1258-76029.jpg?t=st=1713526383~exp=1713529983~hmac=1eb31c33bd9e8f2deccf0af89539580d007d7ea99e3e2574538a58eb8c0322ae&w=1480',
  'https://img.freepik.com/free-photo/young-asian-teenage-girl-surprised-excited-isolated-pink-background_74952-2590.jpg?t=st=1713526870~exp=1713530470~hmac=48c030ab1ccfd7d1911c519425fce57a4921fb7ac3b60afdef5c893d96718c8c&w=1480',
  'https://img.freepik.com/free-photo/portrait-young-asia-lady-with-positive-expression-arms-crossed-smile-broadly-dressed-casual-clothing-looking-camera-pink-background_7861-3201.jpg?t=st=1713525747~exp=1713529347~hmac=c74f5e16b4b20dfd703df3314bf15251d7591dfc9051012dd28f28e0d5052871&w=1480'
]

export default Contacts
