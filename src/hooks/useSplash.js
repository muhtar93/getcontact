import { Animated } from 'react-native'

export default () => {
  const fadeAnim = new Animated.Value(0)

  const onSplash = (navigation) => {
    Animated.timing(
      fadeAnim,
      {
        useNativeDriver: true,
        toValue: 1,
        duration: 3000
      }
    ).start(async () => {
      navigation.reset({
        index: 0,
        routes: [{ name: 'ContactList' }]
      })
    })
  }


  return [onSplash]
}