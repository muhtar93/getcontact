import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Splash from '../screens/splash/Splash'
import AddContact from '../screens/contact/AddContact'
import ContactList from '../screens/contact/ContactList'
import EditContact from '../screens/contact/EditContact'

const Stack = createNativeStackNavigator()

const Main = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen 
          name='Splash' 
          component={Splash} 
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen 
          name='ContactList' 
          component={ContactList} 
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen 
          name='AddContact' 
          component={AddContact} 
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen 
          name='EditContact' 
          component={EditContact} 
          options={{
            headerShown: false
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default Main