import {createSlice} from '@reduxjs/toolkit'
import axios from 'axios'
import { Networks } from '../../consts'
import { showToast } from '../../utils/ShowMessage'

const initialState = {
  data: {},
  isLoading: false,
  error: ''
}

export const addContact = (data, navigation) => {
  return dispatch => {
    dispatch(addContactStart())
    axios.post(Networks.BASE_URL + '/contact', data, 
      {timeout: 60000}
    )
    .then((result) => {
      if(result.status === 201) {
        showToast('Add contact successfully', '', 'success')
        navigation.goBack()
      }
      dispatch(addContactSuccess(result))
    })
    .catch(err => {
      showToast('Add contact failed', err.message, 'danger')
      dispatch(addContactFailure(err))
    })
  }
}

export const addContactReducer = createSlice({
  name: 'addContactReducer',
  initialState,
  reducers: {
    addContactStart: state => {
      state.isLoading = true
    },
    addContactSuccess: (state, action) => {
      state.isLoading = false
      state.data = action.payload
    },
    addContactFailure: (state, action) => {
      state.isLoading = false
      state.error = action.payload
    }
  }
})

export const {addContactStart, addContactSuccess, addContactFailure} = addContactReducer.actions
export default addContactReducer.reducer