import {createSlice} from '@reduxjs/toolkit'
import axios from 'axios'
import { Networks } from '../../consts'

const initialState = {
  data: {},
  isLoading: false,
  error: ''
}

export const getContactById = (id) => {
  return dispatch => {
    dispatch(getContactByIdStart())
    axios.get(Networks.BASE_URL + `/contact/${id}`, {
      timeout: 60000
    })
    .then((result) => {
      dispatch(getContactByIdSuccess(result.data))
    })
    .catch(err => {
      dispatch(getContactByIdFailure(err))
    })
  }
}

export const getContactByIdReducer = createSlice({
  name: 'getContactByIdReducer',
  initialState,
  reducers: {
    getContactByIdStart: state => {
      state.isLoading = true
    },
    getContactByIdSuccess: (state, action) => {
      state.isLoading = false
      state.data = action.payload
    },
    getContactByIdFailure: (state, action) => {
      state.isLoading = false
      state.error = action.payload
    }
  }
})

export const {getContactByIdStart, getContactByIdSuccess, getContactByIdFailure} = getContactByIdReducer.actions
export default getContactByIdReducer.reducer