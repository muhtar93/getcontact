import {createSlice} from '@reduxjs/toolkit'
import axios from 'axios'
import { Networks } from '../../consts'

const initialState = {
  data: {},
  isLoading: false,
  error: ''
}

export const getContact = () => {
  return dispatch => {
    dispatch(getContactStart())
    axios.get(Networks.BASE_URL + '/contact', {
      timeout: 60000
    })
    .then((result) => {
      dispatch(getContactSuccess(result.data))
    })
    .catch(err => {
      dispatch(getContactFailure(err))
    })
  }
}

export const getContactReducer = createSlice({
  name: 'getContactReducer',
  initialState,
  reducers: {
    getContactStart: state => {
      state.isLoading = true
    },
    getContactSuccess: (state, action) => {
      state.isLoading = false
      state.data = action.payload
    },
    getContactFailure: (state, action) => {
      state.isLoading = false
      state.error = action.payload
    }
  }
})

export const {getContactStart, getContactSuccess, getContactFailure} = getContactReducer.actions
export default getContactReducer.reducer