import {createSlice} from '@reduxjs/toolkit'
import axios from 'axios'
import { Networks } from '../../consts'
import { showMessage } from 'react-native-flash-message'
import { showToast } from '../../utils/ShowMessage'

const initialState = {
  data: {},
  isLoading: false,
  error: ''
}

export const deleteContact = (id) => {
  return dispatch => {
    dispatch(deleteContactStart())
    axios.delete(Networks.BASE_URL + `/contact/${id}`,
      {timeout: 60000}
    )
    .then((result) => {
      showToast('Contact deleted successfully', '', 'success')
      dispatch(deleteContactSuccess(result))
    })
    .catch(err => {
      showToast('Delete Failed', err.message, 'danger')
      dispatch(deleteContactFailure(err))
    })
  }
}

export const deleteContactReducer = createSlice({
  name: 'deleteContactReducer',
  initialState,
  reducers: {
    deleteContactStart: state => {
      state.isLoading = true
    },
    deleteContactSuccess: (state, action) => {
      state.isLoading = false
      state.data = action.payload
    },
    deleteContactFailure: (state, action) => {
      state.isLoading = false
      state.error = action.payload
    }
  }
})

export const {deleteContactStart, deleteContactSuccess, deleteContactFailure} = deleteContactReducer.actions
export default deleteContactReducer.reducer