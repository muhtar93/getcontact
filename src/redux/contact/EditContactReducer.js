import {createSlice} from '@reduxjs/toolkit'
import axios from 'axios'
import { Networks } from '../../consts'
import { showToast } from '../../utils/ShowMessage'

const initialState = {
  data: {},
  isLoading: false,
  error: ''
}

export const editContact = (id, data, navigation) => {
  return dispatch => {
    dispatch(editContactStart())
    axios.put(Networks.BASE_URL + `/contact/${id}`, data, 
      {timeout: 60000}
    )
    .then((result) => {
      if(result.status === 201) {
        showToast('Edit contact successfully', '', 'success')
        navigation.goBack()
      }
      dispatch(editContactSuccess(result))
    })
    .catch(err => {
      showToast('Edit contact failed', err.message, 'danger')
      dispatch(editContactFailure(err))
    })
  }
}

export const editContactReducer = createSlice({
  name: 'editContactReducer',
  initialState,
  reducers: {
    editContactStart: state => {
      state.isLoading = true
    },
    editContactSuccess: (state, action) => {
      state.isLoading = false
      state.data = action.payload
    },
    editContactFailure: (state, action) => {
      state.isLoading = false
      state.error = action.payload
    }
  }
})

export const {editContactStart, editContactSuccess, editContactFailure} = editContactReducer.actions
export default editContactReducer.reducer