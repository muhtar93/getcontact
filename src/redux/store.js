import {configureStore} from '@reduxjs/toolkit'
import ContactListReducer from './contact/ContactListReducer'
import ContactDetailReducer from './contact/ContactDetailReducer'
import AddContactReducer from './contact/AddContactReducer'
import DeleteContactReducer from './contact/DeleteContactReducer'
import EditContactReducer from './contact/EditContactReducer'

export const store = configureStore({
  reducer: {
    contactListReducer: ContactListReducer,
    contactDetailReducer: ContactDetailReducer,
    addContactReducer: AddContactReducer,
    deleteContactReducer: DeleteContactReducer,
    editContactReducer: EditContactReducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false,
  })
})