const Colors = {
  primary: '#FFF7E8',
  secondary: '#59A1A5',
  orange: '#FF9E5E',
  pink: '#FF7893',
  brown: '#FFD88F',
  black: '#616161',
  white: '#FFFFFF',
  gray: '#AAAAAA',
  blue: '#0288D1',
  red: '#D32F2F'
}

export default Colors