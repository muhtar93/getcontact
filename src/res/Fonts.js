const Fonts = {
  POPPINS_BOLD: 'Poppins-Bold',
  POPPINS_BOLD_ITALIC: 'Poppins-BoldItalic',
  POPPINS_ITALIC: 'Poppins-Italic',
  POPPINS_LIGHT: 'Poppins-Light',
  POPPINS_LIGHT_ITALIC: 'Poppins-LightItalic',
  POPPINS_MEDIUM: 'Poppins-Medium',
  POPPINS_MEDIUM_ITALIC: 'Poppins-MediumItalic',
  POPPINS_REGULAR: 'Poppins-Regular',
  POPPINS_SEMI_BOLD: 'Poppins-SemiBold',
  POPPINS_SEMI_BOLD_ITALIC: 'Poppins-SemiBoldItalic',
  POPPINS_THIN: 'Poppins-Thin',
  POPPINS_THIN_ITALIC: 'Poppins-ThinItalic'
}

export default Fonts
