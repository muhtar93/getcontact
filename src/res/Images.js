const Images = {
  icGetContact: require('../images/ic_get_contact.png'),
  icAddContact: require('../images/ic_add_contact.png'),
  icEditContact: require('../images/ic_edit_contact.png'),
  icDeleteContact: require('../images/ic_delete_contact.png'),
  icBack: require('../images/ic_back.png'),
  icClose: require('../images/ic_close.png'),
  icNoData: require('../images/ic_no_data.png')
}

export default Images