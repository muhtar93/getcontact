import { View } from 'react-native'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Button, EditText, Gap, Header, Label } from '../../components'
import { ContainerStyle } from '../../styles'
import { Colors } from '../../res'
import { getRandomImage } from '../../utils/CheckImage'
import { Contacts } from '../../consts'
import { addContact } from '../../redux/contact/AddContactReducer'

const AddContact = ({ navigation }) => {
  const dispatch = useDispatch()

  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [age, setAge] = useState('')

  const onSubmitForm = () => {
    const randomImage = getRandomImage(Contacts)

    const data = {
      'firstName': firstName,
      'lastName': lastName,
      'age': age,
      'photo': randomImage
    }

    dispatch(addContact(data, navigation))
  }

  return (
    <View style={ContainerStyle.containerDetail}>
      <Header 
        title='Add Contact'
        onPress={() => navigation.goBack()} 
      />
      <Gap height={16} />
      <Label title='First Name' />
      <Gap height={4} />
      <EditText
        placeholder='John'
        value={firstName}
        onChangeText={(text) => setFirstName(text)}
      />
      <Gap height={8} />
      <Label title='Last Name' />
      <Gap height={4} />
      <EditText
        placeholder='Doe'
        value={lastName}
        onChangeText={(text) => setLastName(text)}
      />
      <Gap height={8} />
      <Label title='Age' />
      <Gap height={4} />
      <EditText
        placeholder='20'
        value={age}
        onChangeText={(text) => setAge(text)}
        keyboardType='number-pad'
      />
      <Gap height={16} />
      <Button
        title='Save'
        bgColor={Colors.secondary}
        textColor={Colors.white}
        onPress={onSubmitForm}
      />
    </View>
  )
}

export default AddContact