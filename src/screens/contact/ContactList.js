import { View, FlatList, RefreshControl } from 'react-native'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getContact } from '../../redux/contact/ContactListReducer'
import { AddButton, ContactItem, EmptyList, Gap, LoadingList, ModalDetail } from '../../components'
import { deleteContact } from '../../redux/contact/DeleteContactReducer'
import { ContainerStyle } from '../../styles'
import Shimmer from 'react-native-shimmer'
import { Networks } from '../../consts'
import { checkImage } from '../../utils/CheckImage'
import { Images } from '../../res'
import { getContactById } from '../../redux/contact/ContactDetailReducer'

const ContactList = ({ navigation }) => {
  const dispatch = useDispatch()
  const contactList = useSelector(state => state.contactListReducer.data?.data)
  const isLoading = useSelector(state => state.contactListReducer.isLoading)

  const [photo, setPhoto] = useState(Networks.DEFAULT_IMAGE)
  const [isVisible, setIsVisible] = useState(false)
  const [name, setName] = useState('')
  const [age, setAge] = useState('')

  useEffect(() => {
    dispatch(getContact())
  }, [])

  useEffect(() => {
    const listener = navigation.addListener('focus', () => {
      dispatch(getContact())
    })

    return listener
  }, [])

  const onPressVisibility = () => {
    setIsVisible(!isVisible)
  }

  const goToContactDetail = (id) => {
    // dispatch(getContactById(id))
    navigation.navigate('EditContact', {id: id})
  }

  const onPressDetail = (item) => {
    setIsVisible(!isVisible)
    setPhoto(checkImage(item.photo) ? item.photo : Networks.DEFAULT_IMAGE)
    setName(`${item.firstName} ${item.lastName}`)
    setAge(`${item.age} Years Old`)
  }

  if (isLoading) {
    return (
      <Shimmer>
        <LoadingList />
      </Shimmer>
    )
  }

  return (
    <View style={ContainerStyle.containerPrimaryColor}>
      <ModalDetail
        isVisible={isVisible}
        source={{ uri: photo }}
        onBackdropPress={onPressVisibility}
        onPressClose={onPressVisibility}
        name={name}
        age={age}
      />
      <AddButton
        onPress={() => navigation.navigate('AddContact')}
      />
      <Gap height={16} />
      <FlatList
        data={contactList}
        renderItem={({ item }) =>
          <ContactItem
            name={`${item.firstName} ${item.lastName}`}
            onPress={() => onPressDetail(item)}
            onPressDelete={() => dispatch(deleteContact(item.id))}
            onPressEdit={() => navigation.navigate('EditContact', {id: item.id})}
          />
        }
        keyExtractor={item => item.id}
        ListEmptyComponent={<EmptyList
          source={Images.icNoData}
          message={'No data available'}
        />}
        contentContainerStyle={{ flexGrow: 1 }}
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={() => dispatch(getContact())} />
        }
      />
    </View>
  )
}

export default ContactList