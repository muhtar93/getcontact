import { View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, EditText, Gap, Header, Label, ModalLoading } from '../../components'
import { ContainerStyle } from '../../styles'
import { Colors } from '../../res'
import { getRandomImage } from '../../utils/CheckImage'
import { Contacts } from '../../consts'
import { getContactById } from '../../redux/contact/ContactDetailReducer'
import { editContact } from '../../redux/contact/EditContactReducer'

const EditContact = ({ route, navigation }) => {
  const dispatch = useDispatch()
  const contactDetail = useSelector(state => state.contactDetailReducer.data?.data)
  const isLoading = useSelector(state => state.contactDetailReducer.isLoading)

  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [age, setAge] = useState('')

  useEffect(() => {
    fetchContactDetail()
  }, [])

  useEffect(() => {
    if (contactDetail) {
      setFirstName(contactDetail.firstName)
      setLastName(contactDetail.lastName)
      setAge(contactDetail.age.toString())
    }
  }, [contactDetail])
  

  const fetchContactDetail = () => {
    dispatch(getContactById(route.params.id))
  }

  const onPressBack = () => {
    navigation.goBack()
  }

  const onSubmitForm = () => {
    const randomImage = getRandomImage(Contacts)

    const data = {
      'firstName': firstName,
      'lastName': lastName,
      'age': age,
      'photo': randomImage
    }

    dispatch(editContact(route.params.id,data, navigation))
  }

  return (
    <View style={ContainerStyle.containerDetail}>
      <ModalLoading 
        visible={isLoading}
      />
      <Header 
        title='Edit Contact'
        onPress={onPressBack} 
      />
      <Gap height={16} />
      <Label title='First Name' />
      <Gap height={4} />
      <EditText
        placeholder='John'
        value={firstName}
        onChangeText={(text) => setFirstName(text)}
      />
      <Gap height={8} />
      <Label title='Last Name' />
      <Gap height={4} />
      <EditText
        placeholder='Doe'
        value={lastName}
        onChangeText={(text) => setLastName(text)}
      />
      <Gap height={8} />
      <Label title='Age' />
      <Gap height={4} />
      <EditText
        placeholder='20'
        value={age}
        onChangeText={(text) => setAge(text)}
        keyboardType='number-pad'
      />
      <Gap height={16} />
      <Button
        title='Save'
        bgColor={Colors.secondary}
        textColor={Colors.white}
        onPress={onSubmitForm}
      />
    </View>
  )
}

export default EditContact