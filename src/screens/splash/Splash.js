import React, { useEffect } from 'react'
import { Animated, Image, Text } from 'react-native'
import { useSplash } from '../../hooks'
import { ContainerStyle, FontStyle, IconStyle } from '../../styles'
import { Images } from '../../res'
import { Gap } from '../../components'

const Splash = ({navigation}) => {
  const [onSplash] = useSplash()

  useEffect(() => {
    onSplash(navigation)
  })

  return (
    <Animated.View style={ContainerStyle.containerCenter}>
      <Image 
        testID='splash-image'
        source={Images.icGetContact}
        style={IconStyle.largeIcon}
        resizeMode='contain'
      />
      <Gap height={16} />
      <Text style={FontStyle.headline}>Get Contact</Text>
    </Animated.View>
  )
}

export default Splash