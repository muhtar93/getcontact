import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../res'

const ContainerStyle = StyleSheet.create({
	containerCenter: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: Colors.white
	},
	containerPrimaryColor: {
		flex: 1,
		backgroundColor: Colors.primary,
		paddingHorizontal: 16,
		paddingTop: 16
	},
	containerDetail: {
		flex: 1,
		backgroundColor: Colors.primary,
		paddingHorizontal: 8
	},
	containerModal: {
		backgroundColor: 'white',
    width: null,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingHorizontal: 16,
    paddingVertical: 8
	},
	containerFlexRow: {
		flexDirection: 'row',
	},
	containerSpaceBetween: {
		justifyContent: 'space-between'
	},
	containerFlexRowCenter: {
		flexDirection: 'row',
		justifyContent: 'center'
	},
	containerAddButton: {
		backgroundColor: Colors.secondary,
		padding: 8,
		borderRadius: 8
	},
	containerLoadingShimmer: {
    flexDirection: 'row',
    padding: 16,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
	containerTitleShimmer: {
    width: 160,
    height: 40,
    backgroundColor: Colors.orange
  },
	containerRadius: {
		height: 120, 
		width: 120, 
		alignSelf: 'center', 
		borderRadius: 120/2
	},
	containerEditText: {
    height: 44,
    borderRadius: 4,
    borderColor: Colors.gray,
    borderWidth: 1,
    paddingHorizontal: 8,
    fontFamily: Fonts.POPPINS_REGULAR,
    fontSize: 14,
    color: 'black'
  },
	containerEmptyData: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
	containerButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    height: 40,
    paddingHorizontal: 8
  },
})

export default ContainerStyle