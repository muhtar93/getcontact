import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../res'

const FontStyle = StyleSheet.create({
	headline: {
		fontFamily: Fonts.POPPINS_BOLD,
		color: Colors.black,
		fontSize: 24
	},
	title: {
		fontFamily: Fonts.POPPINS_REGULAR,
		color: Colors.orange,
		fontSize: 16
	},
	titleDefault: {
    fontSize: 16,
    fontFamily: Fonts.POPPINS_BOLD
  },
	titleBrown: {
		fontFamily: Fonts.POPPINS_REGULAR,
		color: Colors.brown,
		fontSize: 16
	},
	titleSecondary: {
		fontFamily: Fonts.POPPINS_SEMI_BOLD,
		color: Colors.secondary,
		fontSize: 16
	},
	label: {
		fontSize: 14,
		fontFamily: Fonts.POPPINS_REGULAR,
		color: Colors.black
	}
})

export default FontStyle