import { StyleSheet } from 'react-native'

const IconStyle = StyleSheet.create({
	largeIcon: {
		width: 120,
		height: 120
	},
	smallIcon: {
		width: 24,
		height: 24
	},
	verySmallIcon: {
		width: 16,
		height: 16
	}
})

export default IconStyle