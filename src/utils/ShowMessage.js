import { showMessage } from 'react-native-flash-message'

export const showToast = (message, description, type) => {
  showMessage({
    message: message,
    description: description,
    type: type,
    floating: true,
    icon: 'auto',
    position: 'bottom',
    style: { marginRight: 16, marginLeft: 16, marginTop: 16 },
    duration: 3000
  })
}